package com.github.fwfurtado.micro9314.users.login.user;

import com.github.fwfurtado.micro9314.users.domains.User;

import java.util.Optional;

public interface LoginRepository {

    Optional<User> findByEmail(String email);
}
