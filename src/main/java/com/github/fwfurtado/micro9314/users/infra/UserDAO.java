package com.github.fwfurtado.micro9314.users.infra;

import com.github.fwfurtado.micro9314.users.domains.User;
import com.github.fwfurtado.micro9314.users.login.user.LoginRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserDAO implements LoginRepository {

    private static final String VALID_EMAIL = "fernando.furtado@caelum.com.br";
    private static final String PASSWORD = "{bcrypt}$2a$10$sthPqM6qmKDFCdU80HzsFOBTTCoz/ZAbNegRC4bS0oS11meZvHGGS";

    @Override
    public Optional<User> findByEmail(String email) {
        if (VALID_EMAIL.equals(email)) {
            return Optional.of(new User(VALID_EMAIL, PASSWORD, "customer"));
        }

        return Optional.empty();
    }
}
