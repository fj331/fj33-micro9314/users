package com.github.fwfurtado.micro9314.users.login.application;

import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

@Service
public class LoginApplicationService implements ClientDetailsService {

    private final LoginApplicationRepository repository;

    public LoginApplicationService(LoginApplicationRepository repository) {
        this.repository = repository;
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        var service = repository.findByName(clientId).orElseThrow(() -> new ClientRegistrationException("client not found!"));


        return new ClientApplication(service.getName(), service.getSecret(), service.getRoles(), service.getGrantTypes());
    }
}
