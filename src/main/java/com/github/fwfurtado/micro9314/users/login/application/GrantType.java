package com.github.fwfurtado.micro9314.users.login.application;

public enum GrantType {
    PASSWORD("password"),
    CLIENT_DETAILS("client_details"),
    REFRESH_TOKEN("refresh_token");

    private final String grantName;

    GrantType(String grantName) {
        this.grantName = grantName;
    }

    public String getGrantName() {
        return grantName;
    }
}
