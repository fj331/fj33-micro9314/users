package com.github.fwfurtado.micro9314.users.login.application;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ClientApplication implements ClientDetails {
    private final String clientId;
    private final String clientSecret;
    private final String[] scopes;
    private final GrantType[] grants;

    public ClientApplication(String clientId, String clientSecret, String[] scopes, GrantType[] grants) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.scopes = scopes;
        this.grants = grants;
    }

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return Set.of();
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    @Override
    public boolean isScoped() {
        return true;
    }

    @Override
    public Set<String> getScope() {
        return Set.of(scopes);
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return Arrays.stream(grants).map(GrantType::getGrantName).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return Set.of();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return Set.of();
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return 120;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return 240;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return Map.of();
    }
}
