package com.github.fwfurtado.micro9314.users.configuration;

import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.util.JsonParser;
import org.springframework.security.oauth2.common.util.JsonParserFactory;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.util.Map;

public class JwkAccessTokenConverter extends JwtAccessTokenConverter {

    private final RsaSigner signer;
    private final Map<String, String> customHeaders;
    private final JsonParser objectMapper = JsonParserFactory.create();

    public JwkAccessTokenConverter(KeyPair keyPair, Map<String, String> customHeaders) {
        this.customHeaders = customHeaders;
        signer = new RsaSigner((RSAPrivateKey) keyPair.getPrivate());
    }

    @Override
    protected String encode(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        String encoded;

        try {
            encoded = objectMapper.formatMap(getAccessTokenConverter().convertAccessToken(accessToken, authentication));
        }catch (Exception e) {
            throw new IllegalStateException("Cannot convert access token to JSON", e);
        }

        return JwtHelper.encode(encoded, signer, customHeaders).getEncoded();
    }
}
