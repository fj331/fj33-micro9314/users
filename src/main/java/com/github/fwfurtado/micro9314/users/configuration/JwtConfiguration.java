package com.github.fwfurtado.micro9314.users.configuration;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

@Configuration
public class JwtConfiguration {
    private static final String DEFAULT_KID = "forum-id";

    private final KeyPair keyPair;

    public JwtConfiguration(KeyPair keyPair) {
        this.keyPair = keyPair;
    }


    @Bean
    JwkAccessTokenConverter converter() {
        var jwtAccessTokenConverter = new JwkAccessTokenConverter(keyPair, Map.of("kid", DEFAULT_KID));

        jwtAccessTokenConverter.setKeyPair(keyPair);

        return jwtAccessTokenConverter;
    }

    @Bean
    JwtTokenStore tokenStore() {
        return new JwtTokenStore(converter());
    }

    @Bean
    JWKSet jwkSet(KeyPair keyPair) {
        var rsaKey = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
                .keyUse(KeyUse.SIGNATURE)
                .algorithm(JWSAlgorithm.RS256)
                .keyID(DEFAULT_KID).build();

        var jwkSet = new JWKSet(rsaKey);
        return jwkSet;

    }

}
