package com.github.fwfurtado.micro9314.users.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.bootstrap.encrypt.KeyProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;

@Configuration
public class KeyPairConfiguration {

    @Bean
    KeyPair keyPair(@Qualifier("forum") KeyProperties properties) {
        var keyStore = properties.getKeyStore();
        var factory = new KeyStoreKeyFactory(keyStore.getLocation(), keyStore.getSecret().toCharArray());

        return factory.getKeyPair(keyStore.getAlias(), keyStore.getPassword().toCharArray());
    }

    @Bean
    @ConfigurationProperties(prefix = "forum.encrypt")
    @Qualifier("forum")
    KeyProperties keyProperties() {
        return new KeyProperties();
    }


}
