package com.github.fwfurtado.micro9314.users.domains;

public class User {
    private String email;
    private String password;
    private String[] roles;

    public User(String email, String password, String... roles) {
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String[] getRoles() {
        return roles;
    }
}
