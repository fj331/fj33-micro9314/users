package com.github.fwfurtado.micro9314.users.configuration;

import com.github.fwfurtado.micro9314.users.login.application.LoginApplicationService;
import com.github.fwfurtado.micro9314.users.login.user.LoginService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    private final AuthenticationManager authenticationManager;
    private final LoginService loginService;
    private final LoginApplicationService loginApplicationService;
    private final JwkAccessTokenConverter accessTokenConverter;
    private final JwtTokenStore tokenStore;

    public AuthorizationServerConfiguration(AuthenticationManager authenticationManager, LoginService loginService, LoginApplicationService loginApplicationService, JwkAccessTokenConverter accessTokenConverter, JwtTokenStore tokenStore) {
        this.authenticationManager = authenticationManager;
        this.loginService = loginService;
        this.loginApplicationService = loginApplicationService;
        this.accessTokenConverter = accessTokenConverter;
        this.tokenStore = tokenStore;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(loginApplicationService);

    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .accessTokenConverter(accessTokenConverter)
                .tokenStore(tokenStore)
                .userDetailsService(loginService)
                .authenticationManager(authenticationManager);
    }

}
