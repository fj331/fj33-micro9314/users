package com.github.fwfurtado.micro9314.users.infra;

import com.github.fwfurtado.micro9314.users.domain.Service;
import com.github.fwfurtado.micro9314.users.login.application.GrantType;
import com.github.fwfurtado.micro9314.users.login.application.LoginApplicationRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class ServiceDAO implements LoginApplicationRepository {
    private static final String PASSWORD = "{bcrypt}$2a$10$sthPqM6qmKDFCdU80HzsFOBTTCoz/ZAbNegRC4bS0oS11meZvHGGS";
    private static final Map<String, Service> DATABASE = new HashMap<>();

    static {
        DATABASE.computeIfAbsent("frontend", id -> new Service(1L, id, PASSWORD, new String[]{"read:all"}, new GrantType[]{GrantType.PASSWORD, GrantType.REFRESH_TOKEN}));
        DATABASE.computeIfAbsent("posts", id -> new Service(2L, id, PASSWORD, new String[]{"read:all"}, new GrantType[]{GrantType.CLIENT_DETAILS, GrantType.REFRESH_TOKEN}));
        DATABASE.computeIfAbsent("comments", id -> new Service(3L, id, PASSWORD, new String[]{"read:all"}, new GrantType[]{GrantType.CLIENT_DETAILS, GrantType.REFRESH_TOKEN}));
        DATABASE.computeIfAbsent("score", id -> new Service(4L, id, PASSWORD, new String[]{"read:all"}, new GrantType[]{GrantType.CLIENT_DETAILS, GrantType.REFRESH_TOKEN}));
        DATABASE.computeIfAbsent("user", id -> new Service(5L, id, PASSWORD, new String[]{"read:all"}, new GrantType[]{GrantType.CLIENT_DETAILS, GrantType.REFRESH_TOKEN}));
    }


    @Override
    public Optional<Service> findByName(String name) {
        return Optional.ofNullable(DATABASE.get(name));
    }
}
