package com.github.fwfurtado.micro9314.users.domain;


import com.github.fwfurtado.micro9314.users.login.application.GrantType;

public class Service {
    private Long id;
    private String name;
    private String secret;
    private String[] roles;
    private GrantType[] grantTypes;


    public Service(Long id, String name, String secret, String[] roles, GrantType[] grantTypes) {
        this.id = id;
        this.name = name;
        this.secret = secret;
        this.roles = roles;
        this.grantTypes = grantTypes;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSecret() {
        return secret;
    }

    public String[] getRoles() {
        return roles;
    }

    public GrantType[] getGrantTypes() {
        return grantTypes;
    }
}
