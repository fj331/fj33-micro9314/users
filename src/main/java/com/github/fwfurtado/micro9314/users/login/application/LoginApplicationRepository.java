package com.github.fwfurtado.micro9314.users.login.application;

import com.github.fwfurtado.micro9314.users.domain.Service;

import java.util.Optional;

public interface LoginApplicationRepository {
    Optional<Service> findByName(String name);
}
