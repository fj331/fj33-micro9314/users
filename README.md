# Generate asymmetric key
All below commands must be executed into `src/main/resources` directory

## Generate key pair
```bash
keytool -genkeypair -alias forum -keyalg RSA -keypass keypass -keystore forum.jks -storepass storepass
```

## Convert the JKS to PKCS12 format
```bash
keytool -importkeystore -srckeystore forum.jks  -srckeypass keypass -srcalias forum -srcstorepass storepass -destkeystore forum.p12 -deststoretype pkcs12 
```
Type `keypass` as keystore password

## Generate Cert from p12 file
```bash
 keytool -export -alias forum -keystore forum.p12 -keypass keypass -rfc -file forum.cert
```

## Generate public key from cert
```bash
openssl x509 -in forum.cert -pubkey -noout
```
Replace all the resource servers symmetric key from output of the previous command