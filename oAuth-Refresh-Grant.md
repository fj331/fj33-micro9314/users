# Refresh Grant

```mermaid
sequenceDiagram
    participant RO as Resource Owner
    participant CA as Client Application
    participant AS as Authorization Server
    participant RS as Resource Server

    RO->>CA: send request with expired token
    CA->>AS: client_id/client_secret, refresh_token
    AS->>AS: validate credentials and refresh_token
    AS->>CA: send new access token
    CA->>CA: store token
    
   
```

## Usage
```http request
POST /oauth/token 
Authorization: Basic <base64 of client_id:client_secret>
Content-Type: application/x-www-form-urlencoded
Host: <oauth server host:port> 

grant_type=refresh_token&refresh_token=<refresh token>
```