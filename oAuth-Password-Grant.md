# Resource Owner Password Credentials Grant. Aka Password Grant

```mermaid
sequenceDiagram
    # Password Grant
    # usuário/senha
    participant RO as Resource Owner
    participant CA as Client Application
    participant AS as Authorization Server  
    participant RS as Resource Server

    RO->>CA: Login via FB
    CA->>RO: Login FB View
    RO->>CA: username/password
    CA->>AS: username/password client_id/client_secret
    AS->>AS: Validate credentials
    AS->>CA: Access Token
    CA->>CA: Persist Access Token
    CA->>RO: Logged
    RO->>CA: View Pictures
    CA->>RS: GET /pictures Authorization: bearer TOKEN
    RS->>AS: Validate Token
    AS->>RS: TOKEN OK
    RS->>CA: Pictures
```

```http request
POST /oauth/token 
Authorization: Basic <base64 of client_id:client_secret>
Content-Type: application/x-www-form-urlencoded
Host: <oauth server host:port> 

grant_type=password&username=<resource owner username>&password=<resource owner password>
```